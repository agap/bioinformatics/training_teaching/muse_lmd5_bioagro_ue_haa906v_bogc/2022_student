#!/bin/bash 
#
#SBATCH -J gene_family 

#SBATCH -p agap_normal

module purge
module load snakemake/5.13.0 
 
mkdir -p logs/
 
# Print shell commands
#snakemake  --cluster-config cluster_config.yml  --configfile config.yaml --jobs 200 --printshellcmds --dryrun --use-envmodules  --cluster "sbatch  -p {cluster.partition} --ntasks {cluster.ntasks} --mem-per-cpu={cluster.mem-per-cpu} -c {cluster.cpus-per-task} -e {cluster.error} -o {cluster.output}"

# Unlock repository if one job failed
# snakemake  --cluster-config cluster_config.yml  --configfile config.yaml --jobs 200 --unlock  --use-envmodules

# Create DAG file
#snakemake  --cluster-config cluster_config.yml  --configfile config.yaml --jobs 2 --dag  --use-envmodules | dot -Tpng > dag.png

# Run workflow
snakemake --cluster-config cluster_config.yml  --configfile config.yaml --jobs 200  --use-envmodules  --cluster "sbatch  -p {cluster.partition} --ntasks {cluster.ntasks} --mem-per-cpu={cluster.memory} -c {cluster.cpus-per-task} -e {cluster.error} -o {cluster.output}"
