# A snakemake workflow for phylogenetic analysis of protein families

## Objective

Perform phylogenetic analysis of one or more sets of predicted peptide
sequences for homologous genes

## Dependencies

* hmmbuild : Build a profile HMM from an alignment (http://www.csb.yale.edu/userguides/seq/hmmer/docs/node19.html)
* hmmsearch : Search a sequence database with a profile HMM (http://www.csb.yale.edu/userguides/seq/hmmer/docs/node26.html)
* MAFFT : Multiple alignment program for amino acid or nucleotide sequences (https://mafft.cbrc.jp/alignment/software/)
* fasta-fetch : Fetch sequences from a FASTA sequence file. Requires an index file made by fasta-make-index. (https://meme-suite.org/meme/doc/fasta-fetch.html)
* Trimal : Automated removal of spurious sequences or poorly aligned regions from a multiple sequence alignment (http://trimal.cgenomics.org/)
* PhyML : Phylogeny software based on the maximum-likelihood principle (http://www.atgc-montpellier.fr/phyml/usersguide.php)
* RapGreen : Reconcile phylogenetic trees with the corresponding species tree. (http://southgreenplatform.github.io/rap-green/)

## For overview and operating method workflow
See https://gitlab.cirad.fr/umr_agap/bioinformatics/training_teaching/muse_lmd5_bioagro_ue_haa906v_bogc/2022_student/-/wikis/gene-family_4_rice


