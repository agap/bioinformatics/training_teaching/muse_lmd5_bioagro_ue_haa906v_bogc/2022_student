import os 
import sys 
import subprocess,sys,re
from os.path import join,basename
import pandas as pd
from Bio import SearchIO
from Bio import AlignIO
from Bio import SeqIO
from snakemake.io import expand, multiext, glob_wildcards, directory


configfile: "config.yaml"

input_proteome = config["input_proteome"]
input_family = config["family"]

wildcard_constraints:
    database = "|".join(input_proteome.keys()),
    family = "|".join(input_family.keys())
 
# Parse hmmsearch based on evalue cutoff

def parse_hmmsearch_results(hmm_file, pvalue_cutoff):
    list_id = [] 
    results = SearchIO.parse(hmm_file, 'hmmer3-text')
    
    for protein in results: 
        hits = protein.hits
        if len(hits) > 0:
            for i in range(0,len(hits)):
                if hits[i].evalue < pvalue_cutoff:
                    hit_id = hits[i].id 
                    list_id.append(hit_id) 
    return(list_id)
    
     

rule all:
    input: 
        expand("{family}/{family}.nhx" , family=input_family.keys()  )

# Multiple alignment program for amino acid or nucleotide sequences

rule mafft:
    input: 
        family = lambda wildcards: input_family[wildcards.family] 
    output: 
       temp("{family}/profile.aln")
    envmodules:
        config["modules"]["mafft"]
    params:
        maxiterate = config["maxiterate"],
        ep = config["ep"]
    threads: 
        config["threads"]
    shell:"""
        mafft --maxiterate {params.maxiterate}  --ep {params.ep} --thread {threads} {input} > {output}
    """
    
rule trimal:
    input:
        rules.mafft.output
    output: 
        temp("{family}/profile_trimed.aln")
    envmodules:
        config["modules"]["trimal"]
    params:
        advanced_parameter = config["advanced_parameter"]
    threads: 
        config["threads"] 
    shell:"""
        trimal -in {input} -out {output} -fasta {params.advanced_parameter}
    """

rule hmmbuild:
    input:
        rules.trimal.output
    output:
        hmm = temp("{family}/{family}.hmm")
    envmodules:
        config["modules"]["hmmer"]
    threads: 
        config["threads"]
    shell:"""
        hmmbuild --informat afa {output.hmm} {input}
    """
    

rule hmmsearch:
    input:
        db = lambda wildcards: input_proteome[wildcards.database], 
        hmm = rules.hmmbuild.output.hmm
    output:
        hmm = "{family}/{database}.hmm"
    envmodules:
        config["modules"]["hmmer"]
    params:
        evalue = config["evalue"]
    threads: 
        config["threads"]
    shell:"""
        hmmsearch -o {output.hmm} -E {params.evalue} --cpu {threads} {input.hmm} {input.db}
    """


rule hmmparse:
    input:
        hmm = rules.hmmsearch.output.hmm
    output:
        tsv = temp("{family}/{database}.tsv")
    run:
        list_id = [] 
        results = SearchIO.parse(input[0], 'hmmer3-text')
    
        for protein in results: 
            hits = protein.hits
            if len(hits) > 0:
                for i in range(0,len(hits)): 
                    hit_id = hits[i].id 
                    list_id.append(hit_id) 
            #list_id = parse_hmmsearch_results(input.hmm)
            with open(output[0], 'w') as fp:
                fp.write('\n'.join(list_id)) 



############################################
### Create a symoblic link with a nice name for the fasta files.
rule fasta_symlink:
    input:
        lambda wildcards: input_proteome[wildcards.database]
    output:
        temp("logs/{database}.fasta")
    shell:
        "ln -s {input} {output}"

###########################################
### Create fasta index 
rule fasta_index:
    input:
        "logs/{database}.fasta"
    output:
        temp("logs/{database}.fasta.index")
    envmodules:
        config["modules"]["meme"]
    shell:"""
        fasta-make-index {input} -f 
    """


rule fastafetch:
    input: 
        db = "logs/{database}.fasta",
        index=rules.fasta_index.output,
        list=rules.hmmparse.output
    output:
        fasta= temp("{family}/{database}_temp.faa")
    envmodules:
        config["modules"]["meme"]
    threads: 
        config["threads"]
    shell:"""
        fasta-fetch {input.db} -f {input.list} > {output.fasta}
    """

rule add_species:
    input:
        rules.fastafetch.output.fasta
    output:
        temp("{family}/{database}.faa")
    run:
        with open(input[0]) as original, open(output[0], 'w') as corrected:
            records = SeqIO.parse(original, 'fasta')
            for record in records:
                record.id = record.id + "_" + wildcards.database
                record.description = ""
                SeqIO.write(record, corrected, 'fasta')


rule merge_fasta:
    input:
        expand("{{family}}/{database}.faa", database=input_proteome.keys())
    output:
        "{family}/{family}.faa"
    shell:
        "cat {input} > {output}"


        
rule mafft_merge_fasta:
    input:
         rules.merge_fasta.output
    output:
        "{family}/{family}.aln"
    envmodules:
        config["modules"]["mafft"]
    threads: 
        config["threads"]
    shell:"""
        mafft --maxiterate  1000  --ep 0.123 --thread {threads} {input} > {output}
    """
    

rule trimal_family:
    input:
        rules.mafft_merge_fasta.output
    output:
        aln = "{family}/{family}_trimed.aln",
        phylip = "{family}/{family}_trimed.phylip"        
    envmodules:
        config["modules"]["trimal"]
    params:
        advanced_parameter = config["advanced_parameter"]
    threads: 
        config["threads"] 
    shell:"""
        trimal -in {input} -out {output.phylip} -phylip {params.advanced_parameter};
        trimal -in {input} -out {output.aln} {params.advanced_parameter}
    """

        
rule phyml:
    input:
        rules.trimal_family.output.phylip
    output: 
        stats = "{family}/{family}_trimed.phylip_phyml_stats.txt",
        gene_tree = "{family}/{family}_trimed.phylip_phyml_tree.txt"
    envmodules:
        config["modules"]["phyml"]
    params:
        evolution_model = config["evolution_model"],
        tree_topology = config["tree_topology"],
        bootstrap = config["bootstrap"]
    threads: 
        config["threads"] 
    shell:"""
        phyml -i {input} -d aa -m {params.evolution_model} -s {params.tree_topology} -b {params.bootstrap}
    """

rule rapgreen:
    input:
        gene_tree = rules.phyml.output.gene_tree,
        species_tree = config["species_tree"]
    output:
        nhx = "{family}/{family}.nhx",
        stats = "{family}/{family}.stats",
        reconcilied = "{family}/{family}_reconcilied.nhx"
        
    envmodules:
        config["modules"]["java"]
    params:
        gene_threshold = config["gene_threshold"],
        species_threshold = config["species_threshold"],
        polymorphism_threshold = config["polymorphism_threshold"]
    shell:"""
        java -jar scripts/RapGreen.jar -g {input.gene_tree} -s {input.species_tree} -pt {params.polymorphism_threshold} -gt {params.gene_threshold} -st {params.species_threshold} -nhx {output.nhx} -stats {output.stats} -or {output.reconcilied}
    """
